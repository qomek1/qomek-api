using Microsoft.EntityFrameworkCore;

namespace qomek.data.Entities;

public class QomekContext : DbContext
{
    
    protected readonly IConfiguration Configuration;
    public QomekContext(DbContextOptions<QomekContext> context, IConfiguration configuration) : base(context)
    {
        Configuration = configuration;
    }
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseNpgsql(Configuration.GetConnectionString("db"));
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
    }
    //entities
    public DbSet<User> Users { get; set; }
    public DbSet<Blog> Blogs { get; set; }
}