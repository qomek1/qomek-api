using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace qomek.data.Entities;

public partial class BaseEntity
{
    [Key]
    public int Id { get; set; }
    
    public DateTime Created { get; set; }
    
    public DateTime Modified { get; set; }
}