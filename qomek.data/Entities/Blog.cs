using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace qomek.data.Entities;

public class Blog : BaseEntity
{   
    public string Name { get; set; }
    
    public string Body { get; set; }
    
    public int CreateUserId { get; set; }
    
    [ForeignKey(nameof(CreateUserId))]
    public User? CreateUser { get; set; }
}