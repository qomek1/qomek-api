namespace qomek.data.Entities;

public class ChatRoomMessage
{
    public string From { get; set; }
    
    public int Date { get; set; }
    
    public string Message { get; set; }
    
    public string RoomId { get; set; }
}