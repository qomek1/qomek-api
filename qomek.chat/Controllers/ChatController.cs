using System.Text.Json;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using qomek.chat.Hubs;
using qomek.chat.Models;
using StackExchange.Redis;

namespace qomek.chat.Controllers;

[ApiController]
[Route("[controller]")]
public class ChatController : ControllerBase
{
    private IHubContext<ChatHub> _hub;

    public ChatController(IHubContext<ChatHub> hub)
    {
        _hub = hub;
    }
    private static Lazy<ConnectionMultiplexer> lazyConnection = new Lazy<ConnectionMultiplexer>(() => {
        return ConnectionMultiplexer.Connect("localhost,abortConnect=false");
    });

    public static ConnectionMultiplexer Connection {
        get {
            return lazyConnection.Value;
        }
    }
    [HttpGet]
    public async Task<IActionResult> Send(string message, string username)
    {
        var redis = Connection;
        var db = redis.GetDatabase();
        var chatMessage = new  ChatMessage(){ Sender = username, Message = message };
        await db.ListRightPushAsync("chat:messages", JsonSerializer.Serialize(chatMessage));
        await _hub.Clients.All.SendAsync("receiveMessage", message, username);
        return Ok(new { Message = "Request Completed" });
    }

    [HttpPost]
    public List<ChatMessage> Receive()
    {
        var redis = Connection;
        var db = redis.GetDatabase();
        var chatMessages = db.ListRange("chat:messages");
        var ret = new List<ChatMessage>();
        foreach (var message in chatMessages)
        {
            var chatMessage = JsonSerializer.Deserialize<ChatMessage>(message);
            Console.WriteLine($"{chatMessage.Sender}: {chatMessage.Message}");
            ret.Add(chatMessage);
        }
        return ret;
    }
}