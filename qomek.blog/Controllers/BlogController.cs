using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using qomek.blog.Service;
using qomek.data.Entities;
namespace qomek.blog.Controllers;

[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
[ApiController]
[Route("[controller]")]
public class BlogController : ControllerBase
{
    private IBlogService _blogService;
    public BlogController(IBlogService blogService)
    {
        _blogService = blogService;
    }

    [HttpPost]
    public async Task<IActionResult> Add([FromBody]Blog model)
    {
        await _blogService.Add(model);
        return Ok();
    }

    [HttpGet]
    public async Task<IEnumerable<Blog>> GetList()
    {
        return await _blogService.GetAll();
    }
}