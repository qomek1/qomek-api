using Microsoft.EntityFrameworkCore;
using qomek.data.Entities;

namespace qomek.blog.Service;

public interface IBlogService
{
    Task Add(Blog model);

    Task<IEnumerable<Blog>> GetAll();
}

public class BlogService : IBlogService
{
    private QomekContext _context;

    public BlogService(QomekContext context)
    {
        _context = context;
    }

    public async Task Add(Blog model)
    {
        await _context.AddAsync(model);
        await _context.SaveChangesAsync();
    }

    public async Task<IEnumerable<Blog>> GetAll()
    {
        return await _context.Blogs.ToListAsync();
    }
}